import asyncio
import aiohttp_jinja2
import aiohttp_debugtoolbar
import jinja2
from aiohttp_session import session_middleware
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from aiohttp import web
from routes import routes
from middlewares import authorize
from motor import motor_asyncio as ma 
from settings import *
import hashlib


async def on_shutdown(app):
    for ws in app['websocket']:
        await ws.close(code=1001,message='Server shutdown')

middle = [session_middleware(EncryptedCookieStorage(hashlib.sha256(bytes("jhgfdfg", 'UTF8')).digest())), authorize,]
if DEBUG:
    middle.append(aiohttp_debugtoolbar.middleware)
app = web.Application(middlewares=middle)
web.Application
#if DEBUG:
#     aiohttp_debugtoolbar.setup(app)

aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))

# route part
for route in routes:
    app.router.add_route(route[0], route[1], route[2], name = route[3])
app['static_root_url'] = '/static'
app.router.add_static('/ststic', 'static', name='static')
# end route part

# db connect
app.client = ma.AsyncIOMotorClient(MONGO_HOST)
app.db = app.client[MONGO_DB_NAME]
# end db connect

app.on_cleanup.append(on_shutdown)
app['websocket'] = []

log.debug('start server')
web.run_app(app)
log.debug('Stop server end')



