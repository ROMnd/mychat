import json
from time import time 
import bson 
from bson.objectid import ObjectId
import aiohttp_jinja2
from aiohttp import web
from aiohttp_session import get_session
from auth.models import User

def redirect(request, router_name):
    url = request.app.router[router_name].url()
    raise web.HTTPFound(url)

def set_session(swssion, router_name):
    session['user'] = str(user_id)
    session['last_visit'] = time()
    redirect(request, 'main')

def convert_json(message):
    return json.dumps({'error': message})


class Login(web.View):
    @aiohttp_jinja2.template('auth/login.html')
    async def get(self):
        session = await get_session(self.request)
        if session.get('user'):
            redirect(self.request, 'main')
        return ('Please enter login or email')

    async def post(self):
        data = await self.request.post()
        user = User(self.request.app.db, data)
        result = await user.create_user()
        if isinstance(result, ObjectId):
            session = await get_session(self.request)
        else:
            return web.Response(content_type = 'application/json', text=convert_json(result))

class SignIn(web.View):

    @aiohttp_jinja2.template('auth/sign.html')
    async def get(self, **kw):
        session = await get_session(self.request)
        if session.get('user'):
            redirect(self.request, 'main')
        return {'content': 'please enter your data'}

class SignOut(web.View):

    async def get(self, **kw):
        session = await get_session(self.request)
        if session.get('user'):
            del session['user']
            redirect(self.request, 'login')
        else:
            raise web.HTTPForbidden(body=b'Forbidden')


